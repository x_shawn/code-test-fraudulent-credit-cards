package com.afterpay.coding.exercise;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * PoJo encapsulating transaction details, e.g. credit card number, timestamp, and price.
 *
 * @author Shawn
 */
public class Transaction {
    private static final String COMMA = ",";

    private String creditCardNumber;
    private LocalDateTime timestamp;
    private BigDecimal price;

    public Transaction(String creditCardNumber, LocalDateTime timestamp, BigDecimal price) {
        this.creditCardNumber = creditCardNumber;
        this.timestamp = timestamp;
        this.price = price;
    }

    /**
     * Instantiate a {@link Transaction} via parsing a string representation whose format should be the exact as
     * required - i.e. "${hashed_credit_number},${yyyy-MM-ddThh:mm:ss},${numeric_price}"
     *
     * @param raw the raw transaction in string
     * @return a {@link Transaction} instance
     *
     * @throws IllegalArgumentException when the given raw transaction is blank
     * @throws TransactionParseException when any invalid transaction detail found, e.g. malformed timestamp.
     */
    public static Transaction parse(String raw) {
        // input check
        checkArgument(!isBlank(raw), "Raw transaction cannot be blank !");
        checkIfAnyInvalidTransactionInfoFrom(raw);

        String[] details = raw.split(COMMA);
        return new Transaction(
                details[0].trim(), LocalDateTime.parse(details[1].trim()), new BigDecimal(details[2].trim())
        );
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public BigDecimal getPrice() {
        return price;
    }

    private static void checkIfAnyInvalidTransactionInfoFrom(String raw) {
        String[] details = raw.split(COMMA);

        // details amount check
        if (details.length != 3) {
            throw new TransactionParseException(raw, "Raw transaction must consist of the exact 3 parts of information " +
                    "- i.e. hashed credit card number, timestamp, and price; yet the given raw transaction is: " + raw);
        }

        // hashed credit card number check
        if (isBlank(details[0])) {
            throw new TransactionParseException(raw, "Credit card number is BLANK in the given raw transaction: " + raw);
        }

        // timestamp check (both format and content)
        try {
            LocalDateTime.parse(details[1].trim());
        } catch (DateTimeParseException cause) {
            throw new TransactionParseException(raw, cause, "Invalid timestamp: " + details[1]);
        }

        // numeric and non-negative price check
        BigDecimal price;
        try {
            price = BigDecimal.valueOf(Double.valueOf(details[2].trim()));
        } catch (NumberFormatException cause) {
            throw new TransactionParseException(raw, cause, "Invalid price format: " + details[2]);
        }
        if (price.compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new TransactionParseException(raw, "Negative price value: " + price);
        }
    }

    private static boolean isBlank(String content) {
        return isNullOrEmpty(content) || content.trim().isEmpty();
    }
}
