package com.afterpay.coding.exercise;

/**
 * A runtime exception representing any transaction parsing error.
 *
 * @author Shawn
 */
public class TransactionParseException extends RuntimeException {
    private String rawTransactionInfo; // root cause

    public TransactionParseException(String rawTransactionInfo, String message) {
        super(message);
        this.rawTransactionInfo = rawTransactionInfo;
    }

    public TransactionParseException(String rawTransactionInfo, Throwable cause, String message) {
        super(message, cause);
        this.rawTransactionInfo = rawTransactionInfo;
    }

    public String getRawTransactionInfo() {
        return rawTransactionInfo;
    }
}
