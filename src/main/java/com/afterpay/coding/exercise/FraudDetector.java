package com.afterpay.coding.exercise;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Collections.EMPTY_LIST;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toMap;

/**
 * A fraudulent credit card detector who checks if any credit card spending exceeds the threshold on a day.
 *
 * @author Shawn
 */
public class FraudDetector {

    /**
     * Detect those fraudulent credit cards whose total spending exceeds the given threshold, on the given day.
     *
     * @param rawTransactions a list of transactions in string value whose format is "hashed_credit_card,timestamp,price"
     * @param date transaction date without time
     * @param price the threshold spending used to determine if any fraudulent credit card
     *
     * @return a list of fraudulent credit card numbers
     */
    public List<String> findFraudulentCreditCards(List<String> rawTransactions, LocalDate date, BigDecimal price) {
        // no frauds due to nothing to check
        if (isNull(rawTransactions) || rawTransactions.isEmpty()) return EMPTY_LIST;

        // as the term suggested, PRICE tag cannot be negative number
        checkArgument(nonNull(date) && nonNull(price) && (price.compareTo(BigDecimal.valueOf(0)) >= 0));

        // transaction date time period for later filtering
        LocalDateTime start = date.atStartOfDay();
        LocalDateTime end = date.plusDays(1).atStartOfDay().minusSeconds(1);

        List<String> frauds = new ArrayList<>();

        rawTransactions.stream()
                .map(Transaction::parse)
                .filter(transaction -> {
                    LocalDateTime transactionDateTime = transaction.getTimestamp();

                    // only interested in those transactions happened during [start, end]
                    return start.isEqual(transactionDateTime) || end.isEqual(transactionDateTime) ||
                            start.isBefore(transactionDateTime) && end.isAfter(transactionDateTime);
                })
                .collect(toMap(
                        Transaction::getCreditCardNumber, // credit card number as KEY
                        Transaction::getPrice, // total price as VALUE
                        (price1, price2) -> price1.add(price2) // total price aggregation
                ))
                .forEach((creditCard, totalPrice) -> {
                    if (price.compareTo(totalPrice) < 0) frauds.add(creditCard);
                });

        return frauds;
    }
}
