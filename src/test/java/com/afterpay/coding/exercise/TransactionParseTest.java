package com.afterpay.coding.exercise;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.format.DateTimeParseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Unit Test for {@link Transaction#parse(String)}.
 *
 * @author Shawn
 */
public class TransactionParseTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptBlankRawTransaction() {
        Transaction.parse(" ");
    }

    @Test
    public void shouldFailTransactionParseWhenRawTransactionHasLessInfoThanRequired() {
        final String withoutPrice = "10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, ";
        verifyIfTransactionParseFailedDueTo(withoutPrice);
    }

    @Test
    public void shouldFailTransactionParseWhenRawTransactionHasMoreInfoThanRequired() {
        final String withRedundantInfo = "10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, 10.00, sample_transaction";
        verifyIfTransactionParseFailedDueTo(withRedundantInfo);
    }

    @Test
    public void shouldFailTransactionParseWhenRawTransactionHasBlankCreditNumber() {
        final String withBlankCreditCard = " , 2014-04-29T13:15:54, 10.00";
        verifyIfTransactionParseFailedDueTo(withBlankCreditCard);
    }

    @Test
    public void shouldFailTransactionParseWhenRawTransactionHasMalformedTimestampFormat() {
        final String withMalformedTimestamp = "10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29 13:15:54, 10.00";
        verifyIfTransactionParseFailedDueToInvalidTimestampIn(withMalformedTimestamp);
    }

    @Test
    public void shouldFailTransactionParseWhenRawTransactionHasInvalidTimestampValue() {
        final String withInvalidTimestampContent = "10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-32T13:15:54, 10.00";
        verifyIfTransactionParseFailedDueToInvalidTimestampIn(withInvalidTimestampContent);
    }

    @Test
    public void shouldFailTransactionParseWhenRawTransactionHasNonNumericPrice() {
        final String withNonNumericPrice = "10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-32T13:15:54, TenDollars.ZeroCents";
        verifyIfTransactionParseFailedDueTo(withNonNumericPrice);
    }

    @Test
    public void shouldFailTransactionParseWhenRawTransactionHasNegativePriceValue() {
        final String withNegativePriceValue = "10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-30T13:15:54, -10.00";
        verifyIfTransactionParseFailedDueTo(withNegativePriceValue);
    }

    @Test
    public void shouldParseTransactionSuccessfully() {
        final String validCreditCardNumber = "10d7ce2f43e35fa57d1bbf8b1e2";
        final String validTimestamp = "2014-04-30T13:15:54";
        final String validPrice = "10.00";

        Transaction parsed = Transaction.parse(validCreditCardNumber + ", " + validTimestamp + ", " + validPrice);

        assertEquals(validCreditCardNumber, parsed.getCreditCardNumber());
        assertEquals(validTimestamp, parsed.getTimestamp().toString());
        assertEquals(new BigDecimal(validPrice), parsed.getPrice());
    }

    private void verifyIfTransactionParseFailedDueTo(String invalidRawTransactionInfo) {
        try {
            Transaction.parse(invalidRawTransactionInfo);
            fail("should have failed already, due to a transaction parse exception");
        } catch (TransactionParseException ex) {
            assertEquals(invalidRawTransactionInfo, ex.getRawTransactionInfo());
        }
    }

    private void verifyIfTransactionParseFailedDueToInvalidTimestampIn(String invalidRawTransactionInfo) {
        try {
            Transaction.parse(invalidRawTransactionInfo);
            fail("should have failed already, due to a transaction parse exception");
        } catch (TransactionParseException ex) {
            assertEquals(invalidRawTransactionInfo, ex.getRawTransactionInfo());
            assertEquals(DateTimeParseException.class, ex.getCause().getClass());
        }
    }
}
