package com.afterpay.coding.exercise;

import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;
import static java.util.Collections.EMPTY_LIST;
import static org.junit.Assert.*;

/**
 * Unit Test for {@link FraudDetector#findFraudulentCreditCards(List, LocalDate, BigDecimal)}.
 *
 * @author Shawn
 */
public class FraudDetectorTest {
    private static String HASHED_CREDIT_CARD;
    private static String ANOTHER_HASHED_CREDIT_CARD;

    private static LocalDate TODAY_AS_TRANSACTION_DATE;
    private static LocalDate YESTERDAY_AS_TRANSACTION_DATE;

    private static BigDecimal PRICE_THRESHOLD;

    private static List<String> INVALID_TRANSACTIONS_INCLUDED;
    private static List<String> HISTORIC_TRANSACTIONS;
    private static List<String> TODAY_TRANSACTIONS;
    private static List<String> SMALL_TRANSACTIONS;
    private static List<String> VALID_TRANSACTIONS;

    private FraudDetector detector;

    /**
     * For the sake of simplicity, the test data is hard coded here;
     */
    @BeforeClass
    public static void initTestData() {
        HASHED_CREDIT_CARD = "credit_card_number_encoded_by_whatever_hash_algorithm";
        ANOTHER_HASHED_CREDIT_CARD = "yet_another_hashed_credit_card_number";

        TODAY_AS_TRANSACTION_DATE = LocalDate.now();
        YESTERDAY_AS_TRANSACTION_DATE = TODAY_AS_TRANSACTION_DATE.minusDays(1);

        PRICE_THRESHOLD = BigDecimal.valueOf(10.00);

        INVALID_TRANSACTIONS_INCLUDED = ImmutableList.of(
                HASHED_CREDIT_CARD + ", 2014-04-31T13:15:54, 10.00",
                HASHED_CREDIT_CARD + ", 2014-04-30T13:15:54, 10.00"
        );

        HISTORIC_TRANSACTIONS = ImmutableList.of(
                HASHED_CREDIT_CARD + ", " + LocalDateTime.now().minusDays(1).format(ISO_LOCAL_DATE_TIME) + ", 20.00",
                HASHED_CREDIT_CARD + ", " + LocalDateTime.now().minusMonths(1).format(ISO_LOCAL_DATE_TIME) + ", 20.00"
        );
        TODAY_TRANSACTIONS = ImmutableList.of(
                HASHED_CREDIT_CARD + ", " + LocalDateTime.now().format(ISO_LOCAL_DATE_TIME) + ", 20.00"
        );

        SMALL_TRANSACTIONS = ImmutableList.of(
                HASHED_CREDIT_CARD + ", " + LocalDateTime.now().format(ISO_LOCAL_DATE_TIME) + ", 5.00",
                HASHED_CREDIT_CARD + ", " + LocalDateTime.now().format(ISO_LOCAL_DATE_TIME) + ", 5.00"
        );

        VALID_TRANSACTIONS = ImmutableList.of(
                HASHED_CREDIT_CARD + ", " + LocalDateTime.now().minusDays(1).format(ISO_LOCAL_DATE_TIME) + ", 5.00",
                ANOTHER_HASHED_CREDIT_CARD + ", " + LocalDateTime.now().format(ISO_LOCAL_DATE_TIME) + ", 11.00",
                HASHED_CREDIT_CARD + ", " + LocalDateTime.now().minusDays(1).format(ISO_LOCAL_DATE_TIME) + ", 6.00"
        );
    }

    @Before
    public void initTestTarget() {
        detector = new FraudDetector();
    }

    @Test
    public void shouldReturnEmptyFraudsGivenEmptyCreditCardsToDetect() {
        List<String> frauds = detector.findFraudulentCreditCards(EMPTY_LIST, TODAY_AS_TRANSACTION_DATE, PRICE_THRESHOLD);
        assertTrue(frauds.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNullTransactionDate() {
        detector.findFraudulentCreditCards(VALID_TRANSACTIONS, null, PRICE_THRESHOLD);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNullPriceThreshold() {
        detector.findFraudulentCreditCards(VALID_TRANSACTIONS, TODAY_AS_TRANSACTION_DATE, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNegativePriceThreshold() {
        detector.findFraudulentCreditCards(VALID_TRANSACTIONS, TODAY_AS_TRANSACTION_DATE, BigDecimal.valueOf(-10.00));
    }

    @Test
    public void shouldFailFastAtMappingStageGivenAnyInvalidTransactionInfo() {
        try {
            detector.findFraudulentCreditCards(INVALID_TRANSACTIONS_INCLUDED, TODAY_AS_TRANSACTION_DATE, PRICE_THRESHOLD);
            fail("should have failed already, due to a transaction parse exception");
        } catch (TransactionParseException ex) {
            assertEquals(INVALID_TRANSACTIONS_INCLUDED.get(0), ex.getRawTransactionInfo());
        }
    }

    @Test
    public void shouldReturnEmptyFraudsGivenNoTransactionsHappenedOnSpecificDate() {
        List<String> frauds = detector.findFraudulentCreditCards(HISTORIC_TRANSACTIONS, TODAY_AS_TRANSACTION_DATE,
                PRICE_THRESHOLD);
        assertTrue(frauds.isEmpty());

        frauds = detector.findFraudulentCreditCards(TODAY_TRANSACTIONS, YESTERDAY_AS_TRANSACTION_DATE, PRICE_THRESHOLD);
        assertTrue(frauds.isEmpty());
    }

    @Test
    public void shouldReturnEmptyFraudsGivenSumOfAllSelectedTransactionsNotExceededThreshold() {
        List<String> frauds = detector.findFraudulentCreditCards(SMALL_TRANSACTIONS, TODAY_AS_TRANSACTION_DATE,
                PRICE_THRESHOLD);
        assertTrue(frauds.isEmpty());
    }

    @Test
    public void shouldReturnNonEmptyFraudsGivenSumOfAllSelectedTransactionsExceededThreshold() {
        List<String> frauds = detector.findFraudulentCreditCards(VALID_TRANSACTIONS, YESTERDAY_AS_TRANSACTION_DATE,
                PRICE_THRESHOLD);

        assertEquals(1, frauds.size());
        assertEquals(HASHED_CREDIT_CARD, frauds.get(0));
    }
}
