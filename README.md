# Frauds-Detector
This README document aims to walk you through my personal solution - i.e. **Frauds-Detector**, to the coding challenge from Afterpay.

# Requirement
As per the requirement description [here](https://drive.google.com/file/d/0B3eTIijK9JTxT05ZZDNwV01LbUV1S1FCN1lpNzRKSjJHM0cw/view?usp=sharing), a function needs to be realised to detect any fraudulent credit card whose total spending of a given day exceeds a given threshold.

# Solution
To provide a proof of concept (**PoC**) solution, I follow the well-known **KISS** (keep-it-simple-stupid) principle to cover often-seen cases (which can be seen in unit tests). Also, the current version of my solution has taken a few assumptions, which I would elaborate on later.

## Build & Run
Before jumping into the solution *design & implementation* details, here let me firstly explain how to *build & run* **Frauds-Detector** project on your local machine. As such, you can have a *look & feel* prior to its engineering details.

### Pre-requisite
To play around this simple application, you firstly need to ensure the following to be installed  onto your local environment.

 - **Java 8+**
 - **Gradle 4.6+**

Then you can fetch the source code project via this [google drive share link](https://drive.google.com/drive/folders/1SQUUOKZ9NlK7lNdsp1hvMLX_p0xDBqil?usp=sharing); or `git clone` it from [its bitbucket repository](https://bitbucket.org/x_shawn/code-test-fraudulent-credit-cards/src/master/)

When you are under the root directory of fetched source code project, open your *Terminal* app if you are on Mac OS X or any other Unix-alike OS; or *CMD* window if you are using Windows (and finger-crossing ...). After that, take a deep breath, then

### Just Run

    gradle test

The above command goes through both source and test code compile and execution.

## Design
Here I would like to detail the solution design via both Class Model and Operation Sequence.

### Class Model
As per the requirement, it is quite straightforward to have the following.

 - **Transaction**: a PoJo encapsulating the transaction details, e.g. `credit card number`, `timestamp`, `price`.

 - **FraudDetector**: a service implementing the required detection function.

### Operation Sequence
To put the above entities together, we can see how they work with each other via the following sequence diagram.

```mermaid
sequenceDiagram
FraudDetector ->> FraudDetector: checkArgument
FraudDetector ->> Transaction: mapStringToPoJo
Transaction ->> Transaction: parse
FraudDetector ->> FraudDetector: filterTransactionByDate
FraudDetector ->> FraudDetector: reduceTotalPriceToMap
FraudDetector ->> FraudDetector: collectFrauds
```

As you can see from the above operation sequence diagram, the essence of detection logic follows the data streaming pattern in `map-filter-reduce/collect` manner. Specifically,

 1. **Map**: the raw transactions in `String` value are mapped into `Transaction` PoJo instances for later filtering; at this stage, some common data validation is conducted to ensure the correct format and value of timestamp and price for correct data streaming operation, e.g. price aggregation.

 2. **Filter**: the mapped `Transaction` PoJos from above are filtered by the given date.

 3. **Reduce/Collect**: for each specific `Transaction` PoJo, its total prices are aggregated; the final data structure collected from current stream is transformed into a `Map` whose `key` is the credit card number and whose `value` is the aggregated prices.

Finally, we collect all the `key` from the above transformed `Map`, if any `value` exceeds the given threshold.

## Implementation

By following **KISS** principle, the majority of implementation is done by pure Java 8; the only development dependency required is `Google Guava` for some often-used utilities, e.g. `checkArgument`, `isNullOrEmpty`, etc.

As mentioned earlier, I've taken some assumptions during the implementation as follows.

### Assumptions

 - **Non-negative Price**

From the pure data streaming perspective, it does not matter if the price amount is negative, 0, or postive, as long as it is in numeric format. The reason why I take this assumption is from the real-world business perspective. In most cases, the PRICE TAG is either 0 or positive value. Yet you may argue the transaction could be credit amount added into your credit card instead of debit amount subtracted; in that case, I would say the keyword AMOUNT would be more accurate than PRICE. 

Further, as per the original requirement statement, 

> -   price - of format 'dollars.cents'

the format does not specify (+/-) notation, therefore, with the explicit 'price' keyword, I make **Non-negative Price** assumption as part of the precondition check.

 - **Timestamp without timezone**

Similarly, as per the original requirement, there is no timezone information; nor any indication (e.g. location) that requires the timezone process; therefore, the implementation takes the default runtime timezone when constructing and manipulating the datetime.

- **Duplicate transaction is allowed**

Due to the timestamp of a transaction ends at second level, it is possible to have duplicate transactions in the given input. You might believe this should not be allowed, as nearly no one can be that quick to swipe a credit card multiple times within a second. Yet you can also argue the transactions could be auto-generated as a batch job done by an application instead of a human; in that case, the finest timestamp precision is at millisecond level; then is casted into second level as our input. Without the specific context of transaction generation, we can't say the '*duplicate*' transaction is illegal; therefore, back to **KISS** principle, I take this **Duplicate transaction is allowed** assumption during the implementation.

# Test

Due to the simplicity of this project - i.e. a single function, unit test is only conducted for test coverage. There is no need for any integration or load test.

# Future Work

In terms of future work, I would look at it from the perspectives of both functionality and performance.

 - **Functionality**: to ensure the solution correctness, I would try to verify the legitimacy of above assumptions.

 - **Performance**: as you can see, the essence of current solution is that data streaming process - i.e. **map-filter-reduce/collect**, it has `O(n)` (n is the size of given transaction list) time complexity cost (i.e. linear), which means it could potentially suffer from a slow performance as the size of transaction list is increasing. If that does happen (via future monitoring), I would consider using the `ForkJoinPool` based `parallelStream` and benchmark the performance.
